package com.igeek.tmall_fore;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.igeek.tmall_fore.mapper")
public class TmallForeApplication {

    public static void main(String[] args) {
        SpringApplication.run(TmallForeApplication.class, args);
    }

}
