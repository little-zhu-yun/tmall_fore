package com.igeek.tmall_fore.service;

import com.igeek.tmall_fore.domain.Category;

import java.util.List;

/**
 * @Author Administrator
 * @Date 2021/8/19 15:23
 */
public interface TmallHomeCategory {
    List<Category> findAllCategory();
}
