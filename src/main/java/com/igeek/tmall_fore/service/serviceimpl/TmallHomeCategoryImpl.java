package com.igeek.tmall_fore.service.serviceimpl;

import com.igeek.tmall_fore.domain.Category;
import com.igeek.tmall_fore.mapper.TmallHomeCategoryMapper;
import com.igeek.tmall_fore.service.TmallHomeCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Administrator
 * @Date 2021/8/19 15:24
 */
@Service
public class TmallHomeCategoryImpl implements TmallHomeCategory {
    @Autowired
    private TmallHomeCategoryMapper tmallHomeCategoryMapper;
    @Override
    public List<Category> findAllCategory() {
        return tmallHomeCategoryMapper.selectList(null);
    }
}
