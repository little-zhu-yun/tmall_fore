package com.igeek.tmall_fore.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.igeek.tmall_fore.domain.Category;
import org.springframework.stereotype.Repository;

/**
 * @Author Administrator
 * @Date 2021/8/19 15:23
 */
@Repository
public interface TmallHomeCategoryMapper extends BaseMapper<Category> {
}
