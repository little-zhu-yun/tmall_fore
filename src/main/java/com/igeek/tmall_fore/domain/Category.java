package com.igeek.tmall_fore.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author Administrator
 * @Date 2021/8/18 16:45
 */
@Data
public class Category {
    @TableId
    @ApiModelProperty(value = "分类id,主键")
    private Integer id;
    @ApiModelProperty(value = "分类名称")
    private String name;
    @ApiModelProperty(value = "逻辑删除")
    private Integer deleted;
}
