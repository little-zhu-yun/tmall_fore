package com.igeek.tmall_fore.controller;

import com.igeek.tmall_fore.domain.Category;
import com.igeek.tmall_fore.service.serviceimpl.TmallHomeCategoryImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author Administrator
 * @Date 2021/8/19 15:21
 */
@Controller
@Slf4j
public class TmallHomeController {
    @Autowired
    private TmallHomeCategoryImpl tmallHomeCategory;

    @GetMapping("/")
    public String tmallHomePage(){
        return "fore/home";
    }

    @GetMapping("/forehome")
    @ResponseBody
    public List<Category> getCategorylist(){
        List<Category> data=tmallHomeCategory.findAllCategory();
        log.info("data"+data);
        return data;
    }
}
